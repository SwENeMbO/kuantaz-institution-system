import json
from app.models import User
from flask import jsonify, current_app

from app import db

def test_get_all(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid
    """

    response = test_client.get('/api/v1/users/')
    data=json.loads(response.data)

    assert response.status_code == 200
    assert "Users successfully obtained" in data["message"]

def test_create(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (POST)
    THEN check that the response is valid
    """
    newUser = {'name':'Matias', 'surnames':'Gajardo Torres', 'rut':'1234567', 'birthdate':'1996-03-06 15:03:52.960326', 'position':'Backend Developer', 'age':27}
    response = test_client.post('/api/v1/users/', json=newUser)
    data=json.loads(response.data)
    if response.status_code == 201:
        assert response.status_code == 201
        assert "User created successfully" in data["message"]
    elif response.status_code == 403:
        assert response.status_code == 403
        assert "An User with this RUT already exist" in data["description"]
    else:
        assert response.status_code == 200