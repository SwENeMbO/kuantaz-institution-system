from app.models import User


def test_new_user():
    """
    GIVEN a User model
    WHEN a new User is created
    THEN check the email, hashed_password, and role fields are defined correctly
    """
    user = User('Matias', 'Gajardo Torres', "4621048-4", "1996-03-06 15:03:52.960326", "Backend Developer", 27)
    assert user.name == 'Matias'
    assert user.surnames == 'Gajardo Torres'
    assert user.rut == '4621048-4'
    assert user.birthdate == '1996-03-06 15:03:52.960326'
    assert user.position == 'Backend Developer'
    assert user.age == 27