import os
# Determine the folder of the top-level directory of this project
BASEDIR = os.path.abspath(os.path.dirname(__file__))

class Config:
    FLASK_ENV = 'development'
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.getenv('SECRET_KEY', default='BAD_SECRET_KEY')
    # Since SQLAlchemy 1.4.x has removed support for the 'postgres://' URI scheme,
    # update the URI to the postgres database to use the supported 'postgresql://' scheme
    # if os.getenv('DATABASE_URL'):
    #     SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL').replace("postgres://", "postgresql://", 1)
    # else:
    #     SQLALCHEMY_DATABASE_URI = f"sqlite:///{os.path.join(BASEDIR, 'instance', 'app.db')}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # Logging
    LOG_WITH_GUNICORN = os.getenv('LOG_WITH_GUNICORN', default=False)

    @staticmethod
    def init_app(app):
        pass

    # FLASK_ENV = 'development'
    # DEBUG = False
    # TESTING = False

    # @staticmethod
    # def init_app(app):
    #     pass

class DevelopmentConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True
    # TESTING = True

    WTF_CSRF_ENABLED = False
    SECRET_KEY = os.environ.get('SECRET_KEY') or '321654981436519681'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or "postgresql://<username>:<password>@localhost/<database>"
    PORT_APP = 5017
        
    pass
    
class TestingConfig(Config):
    TESTING = True
    # if os.getenv('DATABASE_URL'):
    #     SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL').replace("postgres://", "postgresql://", 1)
    # else:
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{os.path.join(BASEDIR, 'instance', 'app.db')}"

    # SQLALCHEMY_DATABASE_URI = os.getenv('TEST_DATABASE_URI',
    #                                     default=f"sqlite:///{os.path.join(BASEDIR, 'instance', 'test.db')}")
    WTF_CSRF_ENABLED = False
    # FLASK_ENV = 'development'
    # DEBUG = False
    # TESTING = True
    pass

class ProductionConfig(Config):
    FLASK_ENV = 'production'
    pass
    

config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
