from app import create_app
import os 

environment = os.environ.get('environment', 'default')
app = create_app(os.getenv('FLASK_CONFIG') or environment)

if __name__ == '__main__':
    port_app = 8080
    if 'PORT_APP' in app.config and str(app.config['PORT_APP']).isdigit():
        port_app = app.config['PORT_APP']
        app.logger.info('Started on port: '+str(port_app))
        print('Started on port: '+str(port_app))
    app.run(debug=True, host='localhost', port=port_app)