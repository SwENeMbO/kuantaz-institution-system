# Kuantaz Institution REST-API

## Contents

-   [What is this?](#what-is-this)
-   [Task](#what-is-this)
-   [ER diagram](#er-diagram)
-   [Tech Stack](#tech-stack)
-   [Dependencies](#dependencies)
-   [Getting started](#getting-started)
    -   [Requirements](#requirements)
    -   [Install](#install)
    -   [Usage](#usage)
-   [Testing](#dependencies)
-   [Documentation](#dependencies)
-   [TODO Items for POC](#todo-items-for-poc)
-   [References](#references)
-   [Contact](#contact)

## What is this?

Project for Kuantaz company, called Kuantaz Institution REST-API, where used Flask framework. 

## Task

The task is to develop a REST-API that contemplates:

-   models:
    -   **user**, **institution** , **project**

The Rest API must have the following characteristics:

-   Build Rest API with Flask https://flask.palletsprojects.com/en/2.2.x/
-   Use Postgresql Database.
-   Create a CRUD for Institutions
-   Create services to list institutions, projects and users.
-   Create service to list an institution (Filtered by id) with their respective projects and project manager.
-   Create service to list a user (filter by RUT) with their respective projects.
-   Create a service to list institutions where each institution is added to the address the location of google maps example: "https://www.google.com/maps/search/+ address" and the abbreviation of the name (only the first three characters).
-   Create a service to list the projects whose response is the name of the project and the days remaining for its completion.

## ER diagram
![ER Diagram of the REST API](https://i.ibb.co/thykM7x/kuantaz-relational-model-db.png "ER Diagram") 

## Tech Stack:

-   **Web framework:** Flask
-   **ORM:** SQLAlchemy
-   **Database:** PostgresSQL
-   **Parsing/Validation:** flask-expects-json and Flask-WTF
-   **Documentation:** Swagger-UI
-   **API Test:** Postman

## Dependencies

-   `Pip`: Dependency Management under Python
-   `Flask`: Base Web Framework.
-   `werkzeug`: Utility Library under Flask.
-   `flask-expects-json`: Decorator for REST endpoints in flask. Validate JSON request data (Data validation).
-   `Flask-SQLAlchemy`: Flask-SQLAlchemy is an extension for Flask apps that adds support for SQLAlchemy (ORM).
-   `Flask-Migrate`: Flask-Migrate is an extension that handles SQLAlchemy database migrations for Flask applications using Alembic (Database Migrations).
-   `flask-swagger-ui`: Simple Flask blueprint for adding Swagger UI to a flask application.
-   `Flask-WTF`: Simple integration of Flask and WTForms, including CSRF, file upload, and reCAPTCHA (Data validation).
-   `psycopg2`: Psycopg is the most popular PostgreSQL database adapter for the Python programming language
-   `python-dotenv`: Python-dotenv reads key-value pairs from a .env file and can set them as environment variables. It helps in the development of applications following the 12-factor principles.
-   `pytest`: The pytest framework makes it easy to write small, readable tests, and can scale to support complex functional testing for applications and libraries.

## Getting started

## Requirements

    Python 3.11

## Install

1.  Clone the repo

    ```bash
    git clone https://gitlab.com/SwENeMbO/kuantaz-institution-system
    cd kuantaz-institution-system
    ```

2.  Initialize and activate a virtual environment:

    ```bash
    virtualenv venv
    source venv/Scripts/activate # for Windows:
    source venv/bin/activate # for Linux:
    ```

3.  Install dependencies:
    ```bash
    pip install -r requirements.txt # for Windows:
    pip3 install -r requirements.txt # for Linux:
    ```

## Usage

1.  

-   Start Postgres DB and update credentials on `config.py`, for the `DevelopmentConfig`
-   update `SQLALCHEMY_DATABASE_URI` in config.py.
-   SQL alchemy will create the database objects on app creation.


    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or "postgresql://:@/"

2.  Run the development server:

    ```bash
    flask --app kuantaz run --debug
    ```

3.  View the api at  with the Postman collection
    > app/postman

## Testing

To run all the tests at once, use the command:

```bash
    python -m pytest
```

## Documentation

For the documentation on Swagger, we need first to run the project then go to the api docs endpoint

```bash
    flask --app kuantaz run --debug
```

```bash
    localhost:5000/api/v1/docs/
```

## Basic Folder Structure

> kuantaz.py

The major application declaration file.

> config.py

Configuration and Enviroment settings for Flask, PostgreSQL among others.

> app

The Application Layer that defines API controller endpoint, global exceptions and also Request/Response/Presenter/Validator adapters.

> migrations

Necessary files for migrations, generated through Flask-Migrate.

> postman

Postman Endpoints and Environment Variables.

> tests

Test folder, unit folder contains unit test for models with SQLAlchemy and functional test contains functional tests for the endpoints.

> venv

Folder that contains Python and pip executable files to run the project.

> app/**init**.py

File where the application is built (configuration, add plugins, create database)

> app/models.py

File where the models are defined, using SQLAlchemy (specifically Flask-SQLAlchemy)

> app/utils.py

Utility functions to facilitate some actions

> app/user
> app/institution
> app/project

Folders that contains the routes (endpoints), validation schema (for request body JSON validation) and backend validation for the models.

> app/static

Default Flask folder for static resources. In this case, was used to save the swagger YAML file, for documentation.

## TODO Items for POC

-   [x] Input request body on JSON format validation
-   [x] Swagger documentation as part of the API
-   [x] Form validation using Flask-WTF
-   [x] Handle Exceptions
-   [x] Database entities integrated with SQLAlchemy (Postgres DB).
-   [x] Configuration per environment (Development, Testing, and Production)
-   [ ] Tests covering each of the REST API services, with code coverage. Unit tests per module and functional tests.
-   [ ] WSGI Settings
-   [ ] Authentication to Resource API via JWT

## References

-   [Flask](https://flask.palletsprojects.com/en/2.2.x/)
-   [flask-expects-json](https://github.com/fischerfredl/flask-expects-json)
-   [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/)
-   [Flask-Migrate](https://flask-migrate.readthedocs.io/en/latest/)
-   [flask-swagger-ui](https://github.com/sveint/flask-swagger-ui)
-   [Flask-WTF](https://flask-wtf.readthedocs.io/en/1.0.x/)
-   [psycopg2](https://www.psycopg.org/)
-   [python-dotenv](https://github.com/theskumar/python-dotenv)
-   [pytest](https://docs.pytest.org/en/latest/)

## Contact

Matias Gajardo Torres — [LinkedIn](https://www.linkedin.com/in/matias-gajardo-torres-4a9201264/) — matiasgajardotorres@gmail.com