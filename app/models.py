from . import db
from datetime import datetime

from dataclasses import dataclass

@dataclass
class User(db.Model):
    __table_args__ = {"schema": "schema_kuantaz"}
    __tablename__ = 'users'

    id:int = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name:str = db.Column(db.String(64), index=True, nullable=False)
    surnames:str = db.Column(db.String(64), index=True, nullable=False)
    rut:str = db.Column(db.String(32), index=True, unique=True, nullable=False)
    birthdate:str = db.Column(db.DateTime, nullable=False)
    position:str = db.Column(db.String(64), index=True, nullable=False)
    age:int = db.Column(db.Integer, index=True)
    
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)

    projects = db.relationship('Project', backref='responsible_user', lazy='dynamic')

    def __repr__(self):
        return '<The User with rut {}, age {}, named {} and surnamed {} was born on the day {} and has a {} position >'.format(self.rut, self.age, self.name, self.surnames, self.birthdate, self.position)

    def __init__(self, name, surnames, rut, birthdate, position, age):
        self.name = name
        self.surnames = surnames
        self.rut = rut
        self.birthdate = birthdate
        self.position = position
        self.age = age

@dataclass
class Institution(db.Model):
    __table_args__ = {"schema": "schema_kuantaz"}
    __tablename__ = 'institutions'

    id:int = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
    name:str = db.Column(db.String(64), index=True, nullable=False)
    description:str = db.Column(db.String(64), index=True, nullable=False)
    address:str = db.Column(db.String(64), index=True, nullable=False)
    creation_date:str = db.Column(db.DateTime, index=True, nullable=False)
    
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)

    projects = db.relationship('Project', backref='institution', lazy='dynamic')

    def __repr__(self):
        return '<The Institution named "{}", with description: "{}" and address "{}" has the following start_date and end_date respectively: {} - {} >'.format(self.name, self.description, self.address, self.start_date, self.end_date)

    def __init__(self, name, description, address, creation_date):
        self.name = name
        self.description = description
        self.address = address
        self.creation_date = creation_date

@dataclass
class Project(db.Model):
    __table_args__ = {"schema": "schema_kuantaz"}
    __tablename__ = 'projects'

    id:int = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name:str = db.Column(db.String(64), index=True, nullable=False)
    description:str = db.Column(db.String(64), index=True, nullable=False)
    start_date:str = db.Column(db.DateTime, index=True, nullable=False)
    end_date:str = db.Column(db.DateTime, index=True, nullable=False)
    
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)

    user_rut:str = db.Column(db.String, db.ForeignKey('schema_kuantaz.users.rut'))
    institution_id:int = db.Column(db.Integer, db.ForeignKey('schema_kuantaz.institutions.id', ondelete='CASCADE'))

    def __repr__(self):
        return '<The Project named "{}", with description: "{}". Has the following start_date and end_date respectively: {} - {} >'.format(self.name, self.description, self.start_date, self.end_date)

    def __init__(self, name, description, start_date, end_date, user_rut, institution_id):
        self.name = name
        self.description = description
        self.start_date = start_date
        self.end_date = end_date
        self.user_rut = user_rut
        self.institution_id = institution_id