from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length

class CreateForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(), Length(min=1, max=64)])
    surnames = StringField('surnames', validators=[DataRequired(), Length(min=1, max=64)])
    rut = StringField('rut', validators=[DataRequired(), Length(min=1, max=32)])
    birthdate = StringField('birthdate', validators=[DataRequired()])
    position = StringField('position', validators=[DataRequired(), Length(min=1, max=64)])
    age = StringField('age', validators=[DataRequired()])

class UpdateForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(), Length(min=1, max=64)])
    surnames = StringField('surnames', validators=[DataRequired(), Length(min=1, max=64)])
    birthdate = StringField('birthdate', validators=[DataRequired()])
    position = StringField('position', validators=[DataRequired(), Length(min=1, max=64)])
    age = StringField('age', validators=[DataRequired()])