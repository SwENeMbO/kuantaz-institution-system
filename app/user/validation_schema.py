create_user_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'surnames': {'type': 'string'},
        'rut': {'type': 'string'},
        'birthdate': {'type': 'string'},
        'position': {'type': 'string'},
        'age': {'type': 'number'},
    }
}

update_user_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'surnames': {'type': 'string'},
        'birthdate': {'type': 'string'},
        'position': {'type': 'string'},
        'age': {'type': 'number'},
    }
}