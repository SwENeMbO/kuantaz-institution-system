from flask import jsonify, current_app, request, abort, make_response
from datetime import datetime
from ..models import User, Project
from .. import db

from . import user

import json

from flask_expects_json import expects_json
from .validation_schema import create_user_schema, update_user_schema

from .forms_html import CreateForm, UpdateForm

from ..utils import make_jsonify_response

@user.route('/', methods=['GET'])
def query_records():
    users = User.query.all()
    return make_jsonify_response({"message": "Users successfully obtained", "status": 200, "data":users}, 200)

@user.route('/<string:rut>', methods=['GET'])
def query_record(rut=None):
    if rut:
        user = User.query.filter_by(rut=rut).first()
        if user is None:
            abort(code=404, description="User not found")
    return make_jsonify_response({"message": "User successfully obtained", "status": 200, "data":user}, 200)
    
@user.route('/', methods=['POST'])
@expects_json(create_user_schema, check_formats=True)
def create_record():
    form = CreateForm()
    if form.validate():
        data = request.json

        user = User.query.filter_by(rut=data['rut']).first()
        if user:
            abort(code=403, description="An User with this RUT already exist")

        name = data['name']
        surnames = data['surnames']
        rut = data['rut']
        birthdate = data['birthdate']
        position = data['position']
        age = data['age']

        user = User(name=name, surnames=surnames, rut=rut, birthdate=birthdate, position=position, age=age)

        db.session.add(user) 
        db.session.commit()
        
        return make_jsonify_response({"message": "User created successfully", "status": 201, "data":user}, 201)

    return make_jsonify_response({"message": "Error creating user", "status": 400, "errors": form.errors}, 400)

@user.route('/<string:rut>', methods=['PUT'])
@expects_json(update_user_schema, check_formats=True)
def update_record(rut=None):
    form = UpdateForm()
    if form.validate():
        user = User.query.filter_by(rut=rut).first()
        if user is None:
            abort(code=404, description="User not found")

        data = request.json

        user.name = data['name']
        user.surnames = data['surnames']
        user.birthdate = data['birthdate']
        user.position = data['position']
        user.age = data['age']

        db.session.commit()

        return make_jsonify_response({"message": "User updated successfully", "status": 200, "data":user}, 200)
        
    return make_jsonify_response({"message": "Error updating user", "status": 400, "errors": form.errors}, 400)
    
@user.route('/<string:rut>', methods=['DELETE'])
def delete_record(rut):
    user = User.query.filter_by(rut=rut).first()
    if user is None:
        abort(code=404, description="User not found")

    db.session.delete(user)
    db.session.commit()

    return make_jsonify_response({"message": "User deleted successfully", "status": 200, "data":user}, 200)

@user.route('/user-with-projects-in-charge/<string:rut>', methods=['GET'])
def user_with_projects_in_charge(rut):
    user = User.query.filter_by(rut=rut).first()
    if user is None:
        abort(code=404, description="User not found")

    projects = Project.query.all()

    data = []
    user_with_projects = {'user': user,
                        'user_projects': projects
    }
    return make_jsonify_response({"message": "User with projects in charge obtained successfully", "status": 200, "data":user_with_projects}, 200)