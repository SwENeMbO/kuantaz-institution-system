create_or_update_institution_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'description': {'type': 'string'},
        'address': {'type': 'string'},
        'creation_date': {'type': 'string'}
    }
}