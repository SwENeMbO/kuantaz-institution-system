from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length

class CreateOrUpdateForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(), Length(min=1, max=64)])
    description = StringField('description', validators=[DataRequired(), Length(min=1, max=64)])
    address = StringField('address', validators=[DataRequired(), Length(min=1, max=64)])
    creation_date = StringField('description', validators=[DataRequired()])