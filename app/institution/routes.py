from flask import jsonify, current_app, request, abort, make_response
from datetime import datetime
from ..models import Institution, Project
from .. import db

from . import institution

import json

from flask_expects_json import expects_json
from .validation_schema import create_or_update_institution_schema

from .forms_html import CreateOrUpdateForm

from ..utils import make_jsonify_response


@institution.route('/', methods=['GET'])
def query_records():
    institutions = Institution.query.all()
    return make_jsonify_response({"message": "Institutions successfully obtained", "status": 200, "data":institutions}, 200)

@institution.route('/<int:id>', methods=['GET'])
def query_record(id=None):
    if id:
        institution = Institution.query.filter_by(id=id).first()
        if institution is None:
            abort(code=404, description="Institution not found")
    return make_jsonify_response({"message": "Institutions successfully obtained", "status": 200, "data":institution}, 200)
    
@institution.route('/', methods=['POST'])
@expects_json(create_or_update_institution_schema, check_formats=True)
def create_record():
    form = CreateOrUpdateForm()
    if form.validate():
        data = request.json

        name = data['name']
        description = data['description']
        address = data['address']
        creation_date = data['creation_date']

        institution = Institution(name=name, description=description, address=address, creation_date=creation_date)

        db.session.add(institution)
        db.session.commit()

        return make_jsonify_response({"message": "Institution created successfully", "status": 201, "data":institution}, 201)

    return make_jsonify_response({"message": "Error creating institution", "status": 400, "errors": form.errors}, 400)

@institution.route('/<int:id>', methods=['PUT'])
@expects_json(create_or_update_institution_schema, check_formats=True)
def update_record(id):
    form = CreateOrUpdateForm()
    if form.validate():
        data = request.json

        institution = Institution.query.filter_by(id=id).first()
        if institution is None:
            abort(code=404, description="Institution not found")

        institution.name = data['name']
        institution.description = data['description']
        institution.address = data['address']
        institution.creation_date = data['creation_date']

        db.session.commit()

        return make_jsonify_response({"message": "Institution updated successfully", "status": 200, "data":institution}, 200)

    return make_jsonify_response({"message": "Error creating institution", "status": 400, "errors": form.errors}, 400)
    
@institution.route('/<int:id>', methods=['DELETE'])
def delete_record(id):
    institution = Institution.query.filter_by(id=id).first()
    if institution is None:
        abort(code=404, description="Institution not found")

    db.session.delete(institution)
    db.session.commit()

    return make_jsonify_response({"message": "Institution deleted successfully", "status": 200, "data":institution}, 200)

@institution.route('/institutions-with-google-maps-address', methods=['GET'])
def institutions_with_google_maps_address():
    institutions = Institution.query.all()

    data = []
    for institution in institutions:
        institution_name_abbreviation = institution.address[0:3]
        institution_dict = {'institution_name': institution.name,
                        'google_maps_url': "https://www.google.com/maps/search/+{address} {institution_name_abbreviation}".format(address=institution.address, institution_name_abbreviation=institution_name_abbreviation)
        }
        institution_json = json.dumps(institution_dict)
        data.append(institution_dict)
    return make_jsonify_response({"message": "Institutions with his name and google map address obtained successfully", "status": 200, "data":data}, 200)

@institution.route('/institution-with-project-and-responsible/<string:id>', methods=['GET'])
def institution_with_project_and_responsible(id):
    institution = Institution.query.filter_by(id=id).first()
    if institution is None:
        abort(code=404, description="Institution not found")

    projects = Project.query.filter_by(institution_id=id).all()

    data = []
    institution_with_project_and_responsible = {'institution': institution,
                        'institution_projects': projects,
    }
    return make_jsonify_response({"message": "Institutions with projects and responsible user obtained successfully", "status": 200, "data":institution_with_project_and_responsible}, 200)