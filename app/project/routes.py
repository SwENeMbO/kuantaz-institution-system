from flask import jsonify, current_app, request, abort, make_response
from datetime import datetime
from ..models import Project, User, Institution
from .. import db

from . import project

import json

from flask_expects_json import expects_json
from .validation_schema import create_or_update_project_schema

from .forms_html import CreateOrUpdateForm

from ..utils import make_jsonify_response

@project.route('/', methods=['GET'])
@project.route('/<int:id>', methods=['GET'])
def query_records(id=None):
    if id:
        project = Project.query.filter_by(id=id).first()
        if project is None:
            abort(code=404, description="Project not found")
        return jsonify(project)

    projects = Project.query.all()
    return make_jsonify_response({"message": "Projects successfully obtained", "status": 200, "data":projects}, 200)
    
@project.route('/', methods=['POST'])
@expects_json(create_or_update_project_schema, check_formats=True)
def create_record():
    form = CreateOrUpdateForm()
    if form.validate():
        data = request.json

        name = data['name']
        description = data['description']
        start_date = data['start_date']
        end_date = data['end_date']
        responsible_user_rut = data['responsible_user_rut']
        institution_id = data['institution_id']

        user = User.query.filter_by(rut=responsible_user_rut).first()
        if user is None:
            abort(code=404, description="Responsible user not found")
        
        institution = Institution.query.filter_by(id=institution_id).first()
        if institution is None:
            abort(code=404, description="Institution not found")
            
        project = Project(name=name, description=description, start_date=start_date, end_date=end_date, user_rut=responsible_user_rut, institution_id=institution.id)

        db.session.add(project)
        db.session.commit()

        return make_jsonify_response({"message": "Project created successfully", "status": 201, "data":project}, 201)

    return make_jsonify_response({"message": "Error creating Project", "status": 400, "errors": form.errors}, 400)

@project.route('/<int:id>', methods=['PUT'])
@expects_json(create_or_update_project_schema, check_formats=True)
def update_record(id):
    form = CreateOrUpdateForm()
    if form.validate():
        project = Project.query.filter_by(id=id).first()
        if project is None:
            abort(code=404, description="Project not found")

        project.name = data['name']
        project.description = data['description']
        project.start_date = data['start_date']
        project.end_date = data['end_date']
        project.responsible_user_rut = data['responsible_user_rut']
        project.institution_id = data['institution_id']

        db.session.commit()

        return make_jsonify_response({"message": "Project updated successfully", "status": 200, "data":project}, 200)
        
    return make_jsonify_response({"message": "Error creating project", "status": 400, "errors": form.errors}, 400)
    
@project.route('/<int:id>', methods=['DELETE'])
def delete_record(id):
    project = Project.query.filter_by(id=id).first()
    if project is None:
        abort(code=404, description="Project not found")

    db.session.delete(project)
    db.session.commit()

    return make_jsonify_response({"message": "Project deleted successfully", "status": 200, "data":project}, 200)

@project.route('/days-left-for-completion', methods=['GET'])
def days_left_for_completion():
    projects = Project.query.all()

    data = []
    for project in projects:
        days_to_end = project.end_date - datetime.utcnow()
        institution_with_project_and_responsible = {'project_name': project.name,
                                                    'days_left_for_completion': str(days_to_end)
        }
        data.append(institution_with_project_and_responsible)
    return make_jsonify_response({"message": "Projects with the days remaining for completion successfully obtained", "status": 200, "data":data}, 200)    