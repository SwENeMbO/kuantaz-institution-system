from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length

class CreateOrUpdateForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(), Length(min=1, max=64)])
    description = StringField('description', validators=[DataRequired(), Length(min=1, max=64)])
    start_date = StringField('start_date', validators=[DataRequired()])
    end_date = StringField('end_date', validators=[DataRequired()])
    responsible_user_rut = StringField('responsible_user_rut', validators=[DataRequired(), Length(min=1, max=64)])
    institution_id = StringField('institution_id', validators=[DataRequired()]) 