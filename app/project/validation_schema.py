create_or_update_project_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'description': {'type': 'string'},
        'start_date': {'type': 'string'},
        'end_date': {'type': 'string'},
        'responsible_user_rut': {'type': 'string'},
        'institution_id': {'type': 'number'},
    }
}