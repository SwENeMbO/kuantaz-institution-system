from flask import Flask, g, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate #For DB migrations

from config import config

from flask_migrate import Migrate

db = SQLAlchemy()

from flask_swagger_ui import get_swaggerui_blueprint

import sqlalchemy as sa

from .models import User

# Call factory function to create our blueprint
def create_swagger_blue_print(SWAGGER_URL, API_URL):
    return get_swaggerui_blueprint(
        SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
        API_URL,
        config={  # Swagger UI config overrides
            'app_name': "Kuantaz Documentation"
        },
        # oauth_config={  # OAuth config. See https://github.com/swagger-api/swagger-ui#oauth2-configuration .
        #    'clientId': "your-client-id",
        #    'clientSecret': "your-client-secret-if-required",
        #    'realm': "your-realms",
        #    'appName': "your-app-name",
        #    'scopeSeparator': " ",
        #    'additionalQueryStringParams': {'test': "hello"}
        # }
    )
def create_app(config_name='default'):

    app = Flask(__name__)
    # Configure the Flask application
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    migrate = Migrate(app, db)

    #Swagger
    SWAGGER_URL = '/api/v1/docs'  # URL for exposing Swagger UI (without trailing '/')
    API_URL = '/static/swagger.yaml'  # Our API url (can of course be a local resource
    swaggerui_blueprint = create_swagger_blue_print(SWAGGER_URL, API_URL)

    # Error Handling
    from werkzeug.exceptions import HTTPException
    import json
    from flask import make_response
    @app.errorhandler(Exception)
    def handle_exception(e):
    # pass through HTTP errors as json
        if isinstance(e, HTTPException):
            response = e.get_response()
            payload = json.dumps({
                "code": e.code,
                "name": e.name,
                "description": e.description,
            })
            response.data = f"{payload}\n"
            response.content_type = "application/json"
            return make_response(response, e.code)
        # now handle non-HTTP exceptions only
        app.logger.info(str(e))
        response_body = {"code": 500, "message": "Unhandled exception occurred", "details": "The server encountered an internal error and was unable to complete your request. Either the server is overloaded or there is an error in the application. Please contact with admin matiasgajardotorres@gmail.com"}
        return make_response(response_body, 500)

    # Check if the database needs to be initialized
    there_is_users_table = False
    engine = sa.create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    inspector = sa.inspect(engine)
    for table_name in inspector.get_table_names("schema_kuantaz"):
        if table_name == "users":
            there_is_users_table = True
    with app.app_context():
        if there_is_users_table is False:
            db.drop_all()
            db.create_all()
            app.logger.info('Initialized the database!')
        else:
            app.logger.info('Database already contains the users table.')

    with app.app_context():
        from .user import user as user_blueprint
        app.register_blueprint(user_blueprint, url_prefix='/api/v1/users')

        from .institution import institution as institution_blueprint
        app.register_blueprint(institution_blueprint, url_prefix='/api/v1/institutions')

        from .project import project as project_blueprint
        app.register_blueprint(project_blueprint, url_prefix='/api/v1/projects')

        app.register_blueprint(swaggerui_blueprint)

        return app