from flask import make_response, jsonify

def make_jsonify_response(data, status_code):
    response = make_response(jsonify(data),status_code)
    response.headers["Content-Type"] = "application/json"
    return response